from random import normalvariate
import matplotlib.pyplot as plt
from math import pi, exp, sqrt

if __name__ == '__main__':
    sigma = 5
    m = 3
    numbers = [normalvariate(m, sigma) for i in range(100)]

    _, bins, _ = plt.hist(numbers, 30, density=True)

    plt.plot(bins, [1/(sigma * sqrt(2 * pi)) *
                    exp(- (bin - m)**2 / (2 * sigma**2)) for bin in bins],
             linewidth=3, color='b')
    plt.show()

    avg = sum(numbers) / len(numbers)
    sdv = sqrt(sum([(number - avg) ** 2 for number in numbers]) / len(numbers))
    print("AVG: 3 ", avg)
    print("SDV: 5 ", sdv)
    print("Fx: 0.6 ", len(list(filter(lambda x: x < 4.25, numbers))) / len(numbers))
