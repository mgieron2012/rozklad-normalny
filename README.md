# Rozkład normalny


## Zadanie

1. Wygenerować 100 liczb o rozkładzie $`N(m,σ^2)`$ dla ustalonych przez siebie parametrów m i σ.
2. Narysować histogram dla wylosowanej próby oraz wykres gęstości rozkładu $`N(m,σ^2)`$.
3. Dla wylosowanej próby obliczyć wartość średnią i odchylenie standardowe. Porównać wyniki z wartością średnią i odchyleniem standardowym dla zmiennej losowej $`X`$ o rozkładzie $`N(m,σ^2)`$.
4. Wyznaczyć liczbę $`a`$, dla której $`FX(a) = 0.6`$. Następnie sprawdzić, czy dla wylosowanych liczb szansa na wystąpienie liczby, która nie przekracza $`a`$, jest bliska 0,6.

## Odpowiedzi

Ustalone parametry: m=3, σ=5.

Histogram dla 100 wygenerowanych liczb o rozkładzie N(3, 25):

![wykres1](plot1.png)

|	|Wartość oczekiwana	|Wartość obliczona|
|-------|-------------------|-----------------|
|Wartość średnia	|3|	2.647183443960236|
|Odchylenie standardowe	|5|	5.318549418934465|
|Szansa na wylosowanie liczby nieprzekraczającej a = 4,25	|60%|	64%|


Histogram dla 10000 wygenerowanych liczb o rozkładzie N(3, 25):

![wykres2](plot2.png)

|	|Wartość oczekiwana	|Wartość obliczona|
|-------|-------------------|-----------------|
|Wartość średnia	|3|	3.013890691378638|
|Odchylenie standardowe	|5|	4.951677894803121|
|Szansa na wylosowanie liczby nieprzekraczającej a = 4,25	|60%|	59,67%|
